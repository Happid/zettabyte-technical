// Expected Result :
// [
//   { id: '1', name: 'Sherlock', score: 90 },
//   { id: '4', name: 'Budi', score: 85 }
// ]
// Direction :
// Return array of student for score is bigger than mean score (average score)

const students = [
  {
    id: "1",
    name: "Sherlock",
    score: 90,
  },
  {
    id: "2",
    name: "Genta",
    score: 75,
  },
  {
    id: "3",
    name: "Ai",
    score: 80,
  },
  {
    id: "4",
    name: "Budi",
    score: 85,
  },
];

var biggerScore = [];

function result() {
  // Your Code Here
  var totalScore = 0;
  var mean = 0;
  for (var i = 0; i < students.length; i++) {
    totalScore += students[i].score;
  }

  mean = totalScore / students.length;
  for (var i = 0; i < students.length; i++) {
    if (students[i].score > mean) {
      biggerScore.push(students[i]);
    }
  }

  return biggerScore;
}

console.log(result());
