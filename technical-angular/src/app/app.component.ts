import { Component, OnInit } from '@angular/core';
import studentsName from '../assets/students-table-list.json';
import schoolCoorecting from '../assets/school-list.json';
import schoolCorrectorList from '../assets/school-corrector-list.json';

export interface tableStudent {
  student: string | null;
  schoolOrigin: string | null;
  schoolCorrecting: string | null;
  crossCollector: string | null;
}

export interface listSchool {
  _id: string | null;
  short_name: string | null;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'Angular';
  displayedColumns: string[] = [
    'student',
    'schoolOrigin',
    'schoolCorrecting',
    'crossCollector',
  ];
  dataSource: tableStudent[] = [];
  dataSelectSchoolCorrect: listSchool[] = [];

  constructor() {}

  ngOnInit(): void {
    this.GetStudentTable();
    this.GetSchoolCorrecting();
    // this.GetCorrecting();
  }

  GetStudentTable() {
    console.log(studentsName);
    studentsName.map((res) => {
      var tempcrossCollector = '';
      if (
        res.school_correcting_corrector_id.first_name &&
        res.school_correcting_corrector_id.last_name
      ) {
        tempcrossCollector =
          res.school_correcting_corrector_id.first_name +
          ' ' +
          res.school_correcting_corrector_id.last_name;
      }

      this.dataSource.push({
        student: res.student_id.first_name + ' ' + res.student_id.last_name,
        schoolOrigin: res.school_origin_id.short_name,
        schoolCorrecting: res.school_correcting_id.short_name,
        crossCollector: tempcrossCollector,
      });
    });
  }

  GetSchoolCorrecting() {
    schoolCoorecting.map((res) => {
      this.dataSelectSchoolCorrect.push({
        _id: res.school._id,
        short_name: res.school.short_name,
      });
    });
  }

  GetCorrecting() {
    schoolCorrectorList.map((res) => {
      res.cross_correctors.map((res2) => {
        this.slctCross.push({
          full_name: res2.full_name,
          short_name: res.school.short_name,
        });
      });
    });

    console.log(this.slctCross);
  }

  slctCross: any = [];

  handleSchoolCorrecting(event: any, i: any) {
    console.log(schoolCorrectorList);
    console.log(event);

    schoolCorrectorList.map((res, index) => {
      if (event === res.school.short_name) {
        res.cross_correctors.map((res2) => {
          this.slctCross.push({
            full_name: res2.full_name,
          });
        });
      } else {
        this.slctCross.splice(index, 1);
      }
    });
  }
}
