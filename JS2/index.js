// Expected Result = 7
// Direction : Find median of this array
const input = [8, 7, 7, 9, 5, 4, 2, 9];

input.sort(function (a, b) {
  return a - b;
});

function result(input) {
  // Your Code Here
  if (input.length % 2 == 1) {
    return input[input.length / 2 - 0.5];
  } else {
    return (input[input.length / 2] + input[input.length / 2 - 1]) / 2;
  }
}

console.log(result(input));
